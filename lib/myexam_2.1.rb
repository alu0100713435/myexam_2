Node = Struct.new(:value, :next)

class Lista

	attr_accessor :inicio, :final

	def initialize inicio
		@inicio = inicio
		@final = inicio
	end	

	def push(nodo)
		if @final == nil
			@final = nodo
			@inicio = nodo
		else			
			@final.next = nodo
			@final = nodo
		end
	end
	     
	def pop()
		aux = @inicio
		@inicio = @inicio.next		
		return aux.value
	end

	def getFinalValue()
		return @final.value
	end

	def getInicioValue()
		return @inicio.value
	end

	def getInicioNext()
		return @inicio.next
	end

	def getFinalNext()
		return @final.next
	end
end
