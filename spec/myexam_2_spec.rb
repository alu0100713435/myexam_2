require 'lib/myexam_2.rb'
require 'lib/myexam_2.1.rb'

describe SeleccionSimple do
	before :each do
		@p1 = SeleccionSimple.new("Salida del siguiente codigo: Class Xyz \ndef pots \n@nice \nend \nend", ["#<Xyz:0xa000208>","nil","0", "ninguna de las anteriores"])		
		@p2 = SeleccionSimple.new("Definicion de un has en Ruby: hash_raro = \n[1, 2, 3] => Object.new(), \nHash.new => :toto", ["verdadero","Falso"])	
		@p3 = SeleccionSimple.new("Salida del siguiente codigo: Class Array \ndef say_hi \nHEY! \nend \nend", ["1","bob","HEY!","ninguna de las anteriores"])
		@p4 = SeleccionSimple.new("Tipo de objeto en: \nclass Objeto \nend", ["Una instancia de la clase","una constante","un objeto","Ninguna de la anteriores"])
		@p5 = SeleccionSimple.new("Es apropiado que una clase Tablero herede de una clase Juego",["Cierto","Falso"])
	end

	describe "Simple Selection" do

		it "Debe existir una pregunta" do
			@p1.class.should eq(SeleccionSimple)
		end
		it "Se debe invocar a un metodo para obtener la pregunta" do
			@p1.pregunta.should eq("Salida del siguiente codigo: Class Xyz \ndef pots \n@nice \nend \nend")
		end
		it "Deben existir opciones de respuesta" do
			@p1.class.should eq(SeleccionSimple)
		end
		it "Se debe invocar a un metodo para obtener las opciones de respuesta" do
			@p1.respuestas.should eq(["#<Xyz:0xa000208>", "nil", "0", "ninguna de las anteriores"])
		end
		it "Se deben mostrar por la consola formateada la pregunta y las opciones de respuesta" do
			@p1.to_s.should eq("Salida del siguiente codigo: Class Xyz \ndef pots \n@nice \nend \nend\n a:#<Xyz:0xa000208>\n b:nil\n c:0\n d:ninguna de las anteriores")
		end
	end
	before :each do
		@node1 = Node.new(@p1,nil)
		@node2 = Node.new(@p2,nil)
		@node3 = Node.new(@p3,nil)
		@node4 = Node.new(@p4,nil)
		@node5 = Node.new(@p5,nil)
		@lista = Lista.new(@node1)
		@lista1 = Lista.new(nil)
	end
	describe "Preguntas" do
		it "Debe existir un Nodo de la lista con sus datos" do
			@lista.inicio.should_not eq(nil)
		end
		it "Se extrae el primer elemento de la lista" do
			@lista.push(@node2)
			@lista.pop().should eq(@node1.value)
		end
		it "Se puede insertar un elemento" do
			@lista.push(@node3)
			@lista.getFinalValue.should eq(@node3.value)
		end
		it "Se pueden instertar varios elementos" do
			@lista.push(@node4)
			@lista.getFinalValue().should eq(@node4.value)			
			@lista.push(@node5)
			@lista.getFinalValue().should eq(@node5.value)
		end
		it "Debe existir una lista con su cabeza" do
			@lista.inicio.value.should eq(@lista.getInicioValue)
		end
		it "Insercion multiple correcta" do
			@lista1.push(@node1)
			@lista1.push(@node2)
			@lista1.push(@node3)
			@lista1.push(@node4)
			@lista1.push(@node5)
			@lista1.getInicioValue.should eq(@node1.value)
			@lista1.getFinalValue.should eq(@node5.value)
		end
	end
	
end
